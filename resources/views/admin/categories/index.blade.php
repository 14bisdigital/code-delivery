@extends('app')
@section('content')


<div class="container">
    <h3>Categorias</h3>
    <br>
    <a href="{{ Route('admin.categories.create')  }}" class="btn btn-default"> Nova Categoria</a>
    <br><br>
    <div class="col-md-12">

        <table class="table table-bordered table-responsive">
            <thead>
            <tr>
                <th>ID:</th>
                <th>NOME:</th>
                <th>EDITAR OU DELETAR</th>
            </tr>
            </thead>

            <tbody>
            @foreach($categories as $category)
            <tr>
                <td>{{ $category->id }}</td>
                <td>{{ $category->name  }}</td>
                <td>
                    <a href="{{ route('admin.categories.edit', ['id'=>$category->id]) }}" class="btn btn-primary btn-sm">
                        Editar categoria
                    </a>
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>

        {!!  $categories->render()  !!}
        {{-- Páginação --}}

    </div>
</div>

@endsection