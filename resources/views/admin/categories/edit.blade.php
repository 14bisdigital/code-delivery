@extends('app')
@section('content')


<div class="container">
    <h3>Você está editando a categoria: {{ $category->name }}</h3>

   @include('errors._check')

<?php 
    // <!-- Ao utilizar Form::model($category) o databind do laravle já traz todos os dados preenchidos no formulário, seja ele, nome, categoria, valor, id, preço, etc.. Não esquecer de passar o parâmetro id na rota. --> 

    // Não esquecer de abrir outro array em ['admin']
?>

    {!! Form::model($category, ['route'=>['admin.categories.update', $category->id]]) !!}

    @include('admin.categories._form')

    <div class="form-group">
        {!! Form::submit('Editar categoria', ['class'=>'btn btn-primary']) !!}
    </div>

    {!! Form::close() !!}

    </div>
</div>

@endsection