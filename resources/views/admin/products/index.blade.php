@extends('app')
@section('content')


<div class="container">
    <h3>Produtos</h3>
    <br>
    <a href="{{ Route('admin.products.create')  }}" class="btn btn-default"> Novo Produto</a>
    <br><br>
    <div class="col-md-12">

        <table class="table table-bordered table-responsive">
            <thead>
            <tr>
                <th>ID:</th>
                <th>PRODUTO:</th>
                <th>CATEGORIA:</th>
                <th>EDITAR OU DELETAR</th>
            </tr>
            </thead>

            <tbody>
            @foreach($products as $product)
            <tr>
                <td>{{ $product->id }}</td>
                <td>{{ $product->name  }}</td>
                <td>{{ $product->category->name  }}</td>
                <td>
                    <a href="{{ route('admin.products.edit', ['id'=>$product->id]) }}" class="btn btn-primary btn-sm">
                        Editar produto
                    </a>
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>

        {!!  $products->render()  !!}
        {{-- Páginação --}}

    </div>
</div>

@endsection