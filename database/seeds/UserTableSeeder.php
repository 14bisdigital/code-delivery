<?php

use Illuminate\Database\Seeder;
use Delivery\Models\Client;
use Delivery\Models\User; //Utilizando o model user, com isso não é necessário adicionar no factory abaixo
class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Each; A cada 15 usuários será criado 1 client. O usuário fica salvo na memória do php
        // save() responsável por salvar o usuário no banco de dados
        factory(User::class, 15)->create()->each(function($u){
           $u->client()->save(factory(Client::class)->make());
        });
    }
}
