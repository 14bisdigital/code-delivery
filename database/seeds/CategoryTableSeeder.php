<?php

use Delivery\Models\Category;
use Delivery\Models\Product; // Importado a classe para gerar os produtos
use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Category::class, 30)->create()->
        each(function($c) { // Each = Pra cada $c que você criar crie $i (5) produtos
            // A função each cria e salva na memória do php
            // A função Save abaixo, está salvando os produtos no banco de dados
            for ($i=0; $i<=5; $i++) {
                $c->products()->save(factory(Product::class)->make());
            }
        });
    }
}
