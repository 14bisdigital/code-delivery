<?php

namespace Delivery\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Order extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = [
        'client_id',
        'user_deliveryman_id',
        'total',
        'status'
    ];

    public function items() {
        return $this->hasMany(OrderItem::class);
         // Um item pode ter muitos pedidos
    }

    public function develiryman() {
        return $this->belongsTo(User::class);
        // Um pedido só pode ter 1 entregador
    }

}
