<?php

namespace Delivery\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Category extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = [
        'name'
    ];

    public function products() {
        return $this->hasMany(Product::class);
        // hasMany = 1 para muitos. A categoria tem muitos produtos
        // Não esquecer de fazer belongsTo no products = 1 produto para muitas categorias
    }

}
