<?php

namespace Delivery\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class OrderItem extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = [
        'product_id',
        'order_id',
        'price',
        'qtd'
    ];

    public function product() {
        return $this->belongsTo(Product::class);
        // Muitos pedidos para 1 ordem

    }

    public function order() {
        // Pegando a order do produto. 1 Order pode ter vários items
        return $this->belongsTo(Order::class);
    }

}
