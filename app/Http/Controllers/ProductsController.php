<?php

namespace Delivery\Http\Controllers;

use Delivery\Http\Requests;
use Delivery\Http\Requests\AdminCategoryRequest;
use Delivery\Repositories\CategoryRepository;
use Delivery\Repositories\ProductRepository;


class ProductsController extends Controller
{
    /**
     * @var CategoryRepository
     */
    private $repository;
    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    public function __construct(ProductRepository $repository, CategoryRepository $categoryRepository) // Chamando o repository para listar as categorias na view de edição
        {

            $this->repository = $repository;
            $this->categoryRepository = $categoryRepository;
        }

    public function index() {

        $products = $this->repository->paginate(10);

        return view('admin.products.index', compact('products'));
    }

    public function create() {
        return view('admin.categories.create');
    }
        // Store irá receber um request do /categories/create por isso o Request

    public function store(AdminCategoryRequest $request) {
        $data = $request->all();
        $this->repository->create($data);
       return redirect()->route('admin.categories.index');
    }

    public function edit($id) {
        $product = $this->repository->find($id);

        // Chamando as categorias para serem listadas no edit
            $categories = $this->categoryRepository->lists('name', 'id');

        return view('admin.products.edit', compact('product', 'categories'));
    }

    public function update(AdminCategoryRequest $request, $id) {

        $data = $request->all(); // Requisita os dados do banco
        $this->repository->update($data, $id); // Atualiza o registro no banco de dados $id
        return redirect()->route('admin.categories.index'); // Depois de salvar, o usuário será redirecionado par a página de categorias.

    }
}
