<?php

namespace Delivery\Http\Controllers;

use Delivery\Http\Requests;
use Delivery\Http\Requests\AdminCategoryRequest;
use Delivery\Repositories\CategoryRepository;


class CategoriesController extends Controller
{
    /**
     * @var CategoryRepository
     */
    private $repository;

    public function __construct(CategoryRepository $repository)
        {

            $this->repository = $repository;
        }

    public function index() {

        $categories = $this->repository->paginate(100);

        return view('admin.categories.index', compact('categories'));
    }

    public function create() {
        return view('admin.categories.create');
    }
        // Store irá receber um request do /categories/create por isso o Request

    public function store(AdminCategoryRequest $request) {
        $data = $request->all();
        $this->repository->create($data);
       return redirect()->route('admin.categories.index');
    }

    public function edit($id) {
        $category = $this->repository->find($id);
        return view('admin.categories.edit', compact('category'));
    }

    public function update(AdminCategoryRequest $request, $id) {

        $data = $request->all(); // Requisita os dados do banco
        $this->repository->update($data, $id); // Atualiza o registro no banco de dados $id
        return redirect()->route('admin.categories.index'); // Depois de salvar, o usuário será redirecionado par a página de categorias.

    }
}
