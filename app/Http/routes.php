<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/test', function() {
    $repository = app()->make('Delivery\Repositories\CategoryRepository');

    return $repository->all();

});

//Utilizando rotas em grupos e com nomes abaixo 'as'=>'admin.categories.create'

// ROTAS CATEGORIAS
Route::group(['prefix'=>'admin/categories'], function(){
    Route::get('/', ['as'=> 'admin.categories.index', 'uses'=>'CategoriesController@index']);
    Route::get('/create', ['as'=> 'admin.categories.create', 'uses'=>'CategoriesController@create']);
    Route::post('/store', ['as'=> 'admin.categories.store', 'uses'=>'CategoriesController@store']);
    Route::get('/edit/{id}', ['as'=>'admin.categories.edit', 'uses'=> 'CategoriesController@edit']);

    Route::post('/update/{id}', ['as'=>'admin.categories.update', 'uses'=>'CategoriesController@update']);


});


// ROTAS PRODUTOS

Route::group(['prefix'=>'admin/products'], function(){
		Route::get('/', ['as'=> 'admin.products.index', 'uses'=>'ProductsController@index']);
    Route::get('/create', ['as'=> 'admin.products.create', 'uses'=>'ProductsController@create']);
    Route::post('/store', ['as'=> 'admin.products.store', 'uses'=>'ProductsController@store']);
    Route::get('/edit/{id}', ['as'=>'admin.products.edit', 'uses'=> 'ProductsController@edit']);

    Route::post('/update/{id}', ['as'=>'admin.products.update', 'uses'=>'ProductsController@update']);


});










