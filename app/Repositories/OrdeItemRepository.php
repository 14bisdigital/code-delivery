<?php

namespace Delivery\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface OrdeItemRepository
 * @package namespace Delivery\Repositories;
 */
interface OrdeItemRepository extends RepositoryInterface
{
    //
}
