<?php

namespace Delivery\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Delivery\Repositories\OrdeItemRepository;
use Delivery\Models\OrdeItem;
use Delivery\Validators\OrdeItemValidator;

/**
 * Class OrdeItemRepositoryEloquent
 * @package namespace Delivery\Repositories;
 */
class OrdeItemRepositoryEloquent extends BaseRepository implements OrdeItemRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return OrdeItem::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
