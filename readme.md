## Sistema Delivery 
Sistema voltado para delivery, com as seguintes opções
- clientes
- Entregador (Delivery Man)
- Produtos (Id dos produtos)
- categorias

## Framework
Para o desenvolvimento desta aplicação, foi utilizado o laravel 5.1

### Processos e ordem no qual foram criados
#### Criando Base do Sistema
- Instalação e configuração da aplicação
- Alterado o nome da aplicação para Delivery com o comando: php artisan app:name Delivery

- Criação da pasta model: Foi criado a pasta model e movido o arquivo
User.php, cuja estava soldo na pasta /app. Foi corrigido as rotas que necessitavam do arquivo para funcionamento
na pasta 
        
        config Delivery\App\Models\app;

- Criação de Seeders: 
        
     
     CategoryTableSeeder, ProductTableSeeder e UserTableSeeder.

- Products: Criado migration, seeds e dados fictícios com o factory para os produtos, cujo existe relação com a tabela de categorias (Category)

- Clients: Criado migration, seeds e dados fictícios com o factory para os Clientes, cujo existe relação com a tabela de usuários (Users)

- Orders e ordes_item: Criado migration, seeds e dados fictícios com o factory para as ordens, cujo, existe relação com a tabela de produtos e ordens item (Products / orders_item)

#### Repositórios

- Instalação do l5 repositorie - Acesse a url:
        
        https://github.com/andersao/l5-repository

- Digite o seguinte comando no cmd e aguarde instalação: composer require prettus/l5-repository

- Adicione este código nos providers que fica em: App/Config/App.php e encontre Aplication Service Providers:    
 
        Prettus\Repository\Providers\RepositoryServiceProvider::class,

- Depois de registrar o providers, é necessário publicar as configurações. Acesse o terminal e digite o código
   
        php artisan vendor:publish

- Perceba que o sistema copiou o arquivo Repository.php para dentro da pasta App/Config. Acesse este arquivo, navegue até o final e perceba que está sendo utilizado o namespace App\, você deverá trocar para o namespace Delivery\\, que é o nome da sua aplicação

        App/Config/Repository.php/ localize generator e altere rootNamespace de App// para Delivery//
        
        Ainda neste arquivo, altere a rota da Model, que provavelmente será entities. Altere entites para Models

- O sistema adicionou o comando make:repository no php artisan, com isso, iremos criaro s models novamente utilizando o comando php artisan make:repository, porém, ele irá substituir nossos arquivos, então, copiaremos os códigos ja digitados no model e faremos a criação do model com o repository.
    
        php artisan make:repository Nome_do_Model -f
        
        o -f serve para forçar a criação do model, o substituindo
       
       Agora, cole os códigos copiados no novo arquivo de model. Neste exemplo, foi usado o Category
       
- Cuidado: Muito cuidado ao recriar o model User, copie todos os códigos e faça um merge ('União dos arquivos') no novo User.php e verifique se não tem nada duplicado.

- Crie um novo Service Provider com o comando:
            
        php artisan make:provider RepositoryServiceProvider

- Dentro do arquivo RepositoryServiceProvider.php, procure a classe Register e adicione o crie o seguinte código

        $this->app->bind(
            'Delivery\Repositories\CategoryRepository',
            'Delivery\Repositories\CategoryRepositoryEloquent'
       );
       
        Toda a vez que eu chamar a classe CategoryRepository, na verdade, eu quero que você chame o CategoryRepositoryEloquent.
        
- Agora vamos registrar esse provider Navegue até App/Config/App.php e localize Aplication services providers. Cole o código abaixo.


### Sistema Administrativo
- Vamos instalar o scafold que é uma view básica de login e registro do laravel que foi removida na versão 5.1

- Acesse esse link:
        
        https://github.com/bestmomo/scafold
        
- Digite o seguinte comando no prompt de comando
    
        composer require bestmomo/scafold

- Navegue até o arquivo composer.json e adicione o seguinte comando abaixo de "type": "project",
    
         "minimum-stability":"dev",
         
- Navegue até o arquivo app.php em App/Config/App.php e cole o seguinte código abaixo de prettus\Repository...

        Bestmomo\Scafold\ScafoldServiceProvider::class,
        
- Agora vamos publicar as atualizações com o seguinte código
  
        php artisan vendor:publish
        
        
##### Controllers

Insira como parâmetro do método index a tag CategoryRepository $repository e espeficique que irá utilizar use Delivery\Repositories\CategoryRepository;

        App/Http/Controllers/CategoriesController.php insira no método public function index(CategoryRepository $repository) e no início do arquivo insira use Delivery\Repositories\CategoryRepository;

Desisto, muita coisa pra passar kkkk

Instalar o illuminate/html para ajudar na criação de formulários
